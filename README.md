This is a very simple middleware class to check ACL permission strings from the token. Here are some instructions on setting up and using for Laravel 5.2+ (you'll need to translate to Lumen route syntax):

# Composer #

In your composer.json file, you need to add two things:

```
#!php
    "repositories": [
        {
            "url": "git@bitbucket.org:web-artisans/acl-middleware.git",
            "type": "vcs"
        }
    ],
```

and in require:

```
#!php
    "require": {
        "web-artisans/amz-acl-middleware": "dev-master"
    },
```

# Kernel.php #

```
#!php
    protected $routeMiddleware = [
        ...
        'jwt.auth' => \Tymon\JWTAuth\Middleware\GetUserFromToken::class,
        'acl'   => \WebArtisans\JWTAccessControl\Middleware\ACLMiddleware::class,

    ];
```

set up the bottom 'acl'. You will probably need that jwt.auth middleware set that way as well.    

# Tymon JWTAuth library #

You'll notice we are hard-coded to the Tymon JWTAuth library; there are no immediate plans to decouple that. The token as we are using in AMZ Reviews is 'claimString' => [] of strings from the https://drive.google.com/folderview?id=0B800kKB2GNF1ZjVLNkJBMk9hRmc&usp=sharing_eid&ts=568f5bef doc.

## Tymon Service Provider (Laravel) ##

We need to set up the Tymon service provider so that we can access the JWTAuth facade. Add the following line to the `providers` array in your `app.php` config file.

```
#!php
    Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class
```

## Tymon Service Provider (Lumen) ##

We need to register our custom implementation of the Tymon service provider for Lumen compatibility. It is identical to the vendor version, except it does not contain the config publishing functionality, which isn't supported in Lumen.

Add the following line in `bootstrap/app.php` to register the Lumen Service Provider:

```
#!php
    $app->register(WebArtisans\JWTAccessControl\Providers\LumenJWTAuthServiceProvider::class);
```

# Route Definitions (routes.php) #

A route will look like this:

```
#!php
    Route::group(['middleware' => 'acl:amz.reviews'], function () {

        // for testing token authentication
        Route::get('/test', function(){
            return response()->json(['test']);
        });

    });
```

where 'acl' is the alias you added in #2; 'amz.reviews' is the string that will pass to $claimString in the ACLMiddleware class.

The substring search allows you to give the claimString 'amz.reviews' and have it cover 'amz.reviews.user', etc. down the hierarchy.


# Setting up the middleware in Lumen #
The setup process for this middleware differs slightly from Laravel. There is no Kernal.php file in lumen. Instead, you do your setup in bootstrap/app.php.

The first thing to do is enable facades by uncommenting the following line near the top of your file:
```
#!php
$app->withFacades();
```

Next, add the route middleware:
```
#!php

$app->routeMiddleware([
    ...
    'acl'           => WebArtisans\JWTAccessControl\Middleware\ACLMiddleware::class,
]);
```

Next, register the JWTAuthServiceProvider:
```
#!php
$app->register(WebArtisans\JWTAccessControl\Providers\LumenJWTAuthServiceProvider::class);
```

Finally, set a class alias for the JWT auth facade:

```
#!php
class_alias('Tymon\JWTAuth\Facades\JWTAuth', 'JWTAuth');
```

You can test the the middleware by using the above test route, covered to the Lumen syntax. Change out amz.urls.manage.self for the JWT token you wish to test:

```
#!php
$app->group(
    [
        'middleware' => 'acl:amz.urls.manage.self',
    ],
    function () use ($app) {
        // Used to test the authentication middleware
        $app->get('/test', function () {
            return response()->json(['test']);
        });
    }
);
```

You can generate a test token with:
https://bitbucket.org/web-artisans/amz-accounts

Make sure the header of your request has an Authorization/Bearer value:
```
Authorization: Bearer eyJ0eXAiO....
```



# Ownership Verification (Lumen) #
The ACLMiddleware package contains a class for verifying the ownership of a record if the claimString is set to .self. 
It checks the "sub" UUID against a column on the database specified in the dbColumnName parameter.

To use it, add the following to your middleware definitions:
```
#!php

$app->routeMiddleware([
       ...
      'acl.owner'     => WebArtisans\JWTAccessControl\Middleware\ACLMiddlewareOwner::class
]);
```

This middleware accepts several parameters:
```
claimString - The claim string required of this endpoint
resourceName - The table name of the resource you want to check ownership of.
dbColumnName - The name of the column to check. (Optional parameter, "user_id" is default)
```

 The URL parameter that the middleware will look for is the singular version of the model resource.
For example, if you are trying to verify a user owns a url from the urls table, set the resourceName to 'urls', and your route parameter to {url}.

Your routes will look like so:
```
#!php
$app->group(
    [
        'middleware' => 'acl.owner:amz.urls.manage.self,urls'
    ],
    function () use ($app) {
        $app->get('/url/{url}/', ['uses' => 'UrlController@showUrl']);
    }
);
```


@jrmadsen67