<?php

namespace WebArtisans\JWTAccessControl\Providers;

use Tymon\JWTAuth\Providers\JWTAuthServiceProvider;

class LumenJWTAuthServiceProvider extends JWTAuthServiceProvider {
    function boot() {
        $this->bootBindings();

        $this->commands('tymon.jwt.generate');
    }
}