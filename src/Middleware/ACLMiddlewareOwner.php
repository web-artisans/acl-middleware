<?php

namespace WebArtisans\JWTAccessControl\Middleware;

use Closure;

use JWTAuth;
use DB;

class ACLMiddlewareOwner
{

    /**
     * Used to verify that the user has permissions required to modify a record.
     * This will check first if the requested user has permissions to access the site,
     * if so, and the user is in in the 'Self' scope, check to ensure they have ownership to
     * edit the record via checking the "sub" claim in the JWT token against the $ownerColumnName
     * passed into the middleware.
     *
     * If the user has access to the site, and any other permission other than self, it will call the next function
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $claimString The claim string required of this endpoint
     * @param string $resourceName The table name of the resource you want to check ownership of.
     * @param string $dbColumnName The name of the column to check. ("user_id" for example)
     * @return mixed
     */
    public function handle(
        $request,
        Closure $next,
        $claimString,
        $resourceName,
        $dbColumnName = 'user_id'
    ) {
        $payload = JWTAuth::parseToken()->getPayload();
        $claimStringArray = $payload->get('claimStrings');

        // Url parameter name is the singular version of the resource name
        $urlParamName = str_singular($resourceName);

        foreach ($claimStringArray as $value) {
            if (strpos($claimString, $value) !== false) {

                /**
                 * The user has permission to view the site, but if the claim contains ".self", verify
                 * that the user has access to *this* particular record
                 */
                if (strpos($value, '.self') !== false) {

                    // Note: Lumen route() returns an array with the 2nd element containing matches
                    // for URL parameters rather than an object with a parameters() member function.
                    // This is an open bug:
                    // https://github.com/laravel/lumen-framework/issues/291
                    $requestParams = $request->route()[2];

                    // If there is a model reference in the route, try to verify ownership via that method
                    if (array_key_exists($urlParamName, $requestParams)) {
                        $urlParamValue = $requestParams[$urlParamName];

                        // The user has a scope of self but the identifying record couldn't be parsed out. They shouldn't have access.
                        if (is_null($urlParamValue)) {
                            return response('', 403);
                        }

                        // Query for the resource with the URL parameter as the primary key
                        $discoveredObject = DB::table($resourceName)->find($urlParamValue);

                        // If the object came back empty, or doesn't have the property set correctly
                        // for the column identifying the owner, return a 403
                        if (empty($discoveredObject) || property_exists($discoveredObject, $dbColumnName) === false) {
                            return response('', 403);
                        }

                        // Finally, check that the user owns this record, if they do, continue. Otherwise, 403
                        if ($discoveredObject->$dbColumnName == $payload->get('sub')) {
                            return $next($request);
                        }
                    }


                    // If there is a user UUID in the route, lets use that instead. Check the sub claim on the payload
                    // make sure they match. If they do, continue to the next function. Otherwise, 403. We do this before the DB check to save some time.

                    if (array_key_exists('user', $requestParams)) {
                        if ($payload->get('sub') != $requestParams['user']) {
                            return response('', 403);
                        }

                        return $next($request);
                    }

                    // If we get this far, it means that the user has a "self" permission, but doesn't own the record
                    // They do not have permission, or something else is wrong, so return a 403
                    return response('', 403);
                }

                // If you get to this point, you have permission to view this page, but permissions are not within the "self" scope.
                return $next($request);
            }
        }

        // Nothing was found which would say you have access to this site. Return an error message
        return response('Unauthorized.', 401);
    }
}