<?php

namespace WebArtisans\JWTAccessControl\Middleware;

use Closure;

use JWTAuth;

class ACLMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $claimString)
    {

        $payload = JWTAuth::parseToken()->getPayload();
        $claimStringArray = $payload->get('claimStrings');

        foreach ($claimStringArray as $value) {            
            if (strpos($claimString, $value) !== false)
            {
                return $next($request);
            }    
        }

        return response('Unauthorized.', 401);
    }
}
